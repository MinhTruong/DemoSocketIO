//
//  ViewController.swift
//  DemoSocketIO
//
//  Created by TruongVO07 on 7/4/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

import UIKit
import SocketIOClientSwift


class ViewController: UIViewController {
  @IBOutlet weak var hostTextField: UITextField!
  @IBOutlet weak var connectButton: UIButton!
  @IBOutlet weak var statusLabel: UILabel!
  
  //Socket
  var socket: SocketIOClient?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    hostTextField.text = "http://10.10.6.82:5007"
    hostTextField.font = UIFont.systemFontOfSize(13)
    statusLabel.font = UIFont.systemFontOfSize(13)
    statusLabel.textAlignment = .Left
    connectButton.titleLabel?.font = UIFont.systemFontOfSize(13)
    connectButton.addTarget(self, action: #selector(ViewController.connect), forControlEvents: .TouchUpInside)
    updateConnectTitle()
  }
  
  func updateConnectTitle() {
    if let sk = socket {
      connectButton.setTitle("Disconnect", forState: .Normal)
      statusLabel.text = sk.status.description
    } else {
      connectButton.setTitle("Connect", forState: .Normal)
      statusLabel.text = "Disconnect"
    }
  }
  
  func connect() {
    if let _ = socket {
      disConnect()
    } else if !hostTextField.text!.isEmpty {
      if let url = NSURL(string: hostTextField.text!) {
        socket = SocketIOClient(socketURL: url, options: [.ForceNew(true), .ForcePolling(true), .ForceWebsockets(true)])
        print("==> Host socket: \(socket!.socketURL)")
        onAndConnect()
      }
    }
  }
  
  private func onAndConnect() {
    if let sk = socket {
      if sk.status == SocketIOClientStatus.Connected || sk.status == SocketIOClientStatus.Connecting {
        return
      } else {
        sk.on("connect") { (data, ack) in
          self.updateConnectTitle()
        }
        
        sk.on("disconnect") { (data, ack) in
          self.socket = nil
          self.updateConnectTitle()
        }
        
        sk.on("reconnect", callback: { (data, ack) in
          self.updateConnectTitle()
        })
        
        sk.on("error") {data, ack in
          let ms = "\(data)"
          self.statusLabel.text = ms
        }
        sk.connect()
      }
    }
  }
  
  func disConnect() {
    socket!.disconnect()
    socket = nil
  }
}

